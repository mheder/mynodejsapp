# mynodejsapp

This **mynodejsapp** project demonstrates the GitLab CI/CD capabilites, regarding:
* **container creation** stages creates:
  * base container from scratch
  * application container on top of the base container with docker build and **http_server.js** started by the container
* **test** simple nodejs application with test. Source: https://github.com/RisingStack/nodehero-testing
* **deploy** the created image from gitlab registry to docker swarm environment and **http_server.js** automatically started in this stage

## create containers
### build base image
**build_base_container** stage debootstrap and publish (*container/build_base.sh*) Debian Buster based container what can be used in the following stages. Because this is a demo only a *general purpose* container created. For a real life solutuin I suggest to use different layers or different containers for different purpose.
**build_app-image** stage use a simple *Dockerfile* to create and push the container with the nodejs application. The *Dockerfile* dynamically created based on the project and branch name (*container/build_app-image.sh*).
### test
The **test_nodejs** stage a copy-paste of gergelyke's repository. Just simply install and test nodejs.

## deploy
**Prerequirements:**
- Docker swarm cluster
- gitlab-runner installed on Docker swarm

For this demo I just installed the gitlab-runner on the same machine where the Docker swarm configured. In this case the containers started by the runner has direct access to the same Docker socket so the gitlab CI/CD can access swarm.

The *container/deploy_app-image.sh* just dynamically creates a *docker-compose.yml* based on the project and branch name and finally deploy HA proxy and the application image into Docker swarm. The *http_server.js* automatically started. After the successful run few minutes required for swarm to apply the changes. You can test the result here: ~~CENSORED~~

## Rules

* The base container only created for tags.
* The **deploy** stage only starts for the **dev** branch.
* The master branch protected, not allowed to directly push there.
* Tags are protected, not allowed to remove and only maintainers can create new.