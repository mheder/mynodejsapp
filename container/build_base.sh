#!/bin/sh
set -eux

build_container ()
{
  cdebootstrap --keyring /etc/apt/trusted.gpg.d/debian-archive-"${os_release}"-stable.gpg --flavour=minimal --include=apt-transport-https,ca-certificates,nano,perl-modules,sed,wget,mount,nodejs,npm,docker.io --arch=amd64 "${os_release}" debian-"${os_release}" http://ftp.at.debian.org/debian/
  chroot debian-"${os_release}" mount -t proc proc /proc
  chroot debian-"${os_release}" mount -t sysfs sysfs /sys
  chroot debian-"${os_release}" mount -t devtmpfs udev /dev
  chroot debian-"${os_release}" mount -t devpts devpts /dev/pts
  chroot debian-"${os_release}" mount -t tmpfs tmpfs /dev/shm
  chroot debian-"${os_release}" update-ca-certificates
  chroot debian-"${os_release}" umount /proc /sys /dev/pts /dev/shm /dev
}

if [ -z "${CI_COMMIT_TAG+x}" ] ; then
  for os_release in buster
  do
    : ${DOCKERIMG_PATH:="${CI_REGISTRY}/${CI_PROJECT_PATH}/${os_release}-${CI_PROJECT_NAME}"}
    build_container
    tar -c --directory debian-"${os_release}" . | docker import - "${DOCKERIMG_PATH}:${CI_COMMIT_REF_SLUG}"
    docker push "${DOCKERIMG_PATH}:${CI_COMMIT_REF_SLUG}"
  done
else
  for os_release in buster
  do
    : ${DOCKERIMG_PATH:="${CI_REGISTRY}/${CI_PROJECT_PATH}/${os_release}-${CI_PROJECT_NAME}"}
    major_tag=$(echo "${CI_COMMIT_TAG}" | cut -d. -f1)
    minor_tag=$(echo "${CI_COMMIT_TAG}" | cut -d. -f1,2)
    patch_tag=$(echo "${CI_COMMIT_TAG}" | grep -E '^v[0-9]+\.[0-9]+(\.[0-9]+)?(-.+)?$')
      if [ -z "${patch_tag}" ] ; then
        printf 'Error: the latest '"${CI_COMMIT_TAG}"' tag is not starts with v[0-9].[0-9]%s\n'
        exit 1
      fi
    build_container
    tar -c --directory debian-"${os_release}" . | docker import - "${DOCKERIMG_PATH}":tagged
    for tag in "${major_tag}" "${minor_tag}" "${patch_tag}" latest
    do
      docker tag "${DOCKERIMG_PATH}":tagged "${DOCKERIMG_PATH}:${tag}"
      docker push "${DOCKERIMG_PATH}:${tag}"
    done
  done
fi
