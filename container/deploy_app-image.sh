#!/bin/sh
set -eux

: ${DOCKER-COMPOSE_FILE:="docker-compose.yml"}

cat <<EOF >"${DOCKER-COMPOSE_FILE}"
version: '3.7'

services:
  mynodejsapp:
   image: "${CI_REGISTRY}/${CI_PROJECT_PATH}/nodejs-app-${CI_PROJECT_NAME}:${CI_COMMIT_REF_SLUG}"
   ports:
     - 1080
   environment:
     - SERVICE_PORTS=1080
   deploy:
     replicas: 3
     update_config:
       parallelism: 1
       delay: 10s
     restart_policy:
       condition: on-failure
       max_attempts: 3
       window: 120s
   networks:
     - demo

  proxy:
    image: dockercloud/haproxy
    depends_on:
      - mynodejsapp
    environment:
      - BALANCE=leastconn
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    ports:
      - 8080:80
    networks:
      - demo
    deploy:
      placement:
        constraints: [node.role == manager]

networks:
  demo:
    driver: overlay
EOF

docker stack deploy --with-registry-auth --compose-file="${DOCKER-COMPOSE_FILE}" demo

