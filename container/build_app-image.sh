#!/bin/sh
set -eux

: ${DOCKERAPPIMG_PATH:="${CI_REGISTRY}/${CI_PROJECT_PATH}/nodejs-app-${CI_PROJECT_NAME}:${CI_COMMIT_REF_SLUG}"}

cat <<EOF >Dockerfile
FROM "${CI_REGISTRY}/${CI_PROJECT_PATH}/buster-${CI_PROJECT_NAME}:latest"

# Create app directory
WORKDIR /usr/src/app

COPY *.json *.js lib/* ./

RUN npm install

EXPOSE 1080
CMD [ "node", "http_server.js" ]
EOF

docker build -t "${DOCKERAPPIMG_PATH}" .
docker push "${DOCKERAPPIMG_PATH}"
